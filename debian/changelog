biomaj3 (3.1.24-2) unstable; urgency=medium

  * Team upload.
  * Remove dependenct on python3-mock

 -- Alexandre Detiste <tchet@debian.org>  Sat, 14 Dec 2024 20:17:35 +0100

biomaj3 (3.1.24-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Fri, 19 Apr 2024 13:13:23 +0900

biomaj3 (3.1.23-2) unstable; urgency=medium

  * Team upload.
  * Remove need for future
    Closes: #1058565
  * Standards-Version: 4.6.2 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 16 Dec 2023 09:36:54 +0100

biomaj3 (3.1.23-1) unstable; urgency=medium

  * New upstream release using pytest vs nose (Closes: #1018320).

 -- Olivier Sallou <osallou@debian.org>  Mon, 29 Aug 2022 13:44:33 +0000

biomaj3 (3.1.22-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Re-enable influxdb which was fixed
  * Enable at least superfluous autopkgtest
  * Fix lintian-overrides
  * DEP3

 -- Andreas Tille <tille@debian.org>  Thu, 30 Jun 2022 16:48:27 +0200

biomaj3 (3.1.18-2) unstable; urgency=medium

  * Remove drmaa support (optional feature) due to drmaa package
    migration issue on 0.7.9-2

 -- Olivier Sallou <osallou@debian.org>  Fri, 19 Feb 2021 10:25:44 +0000

biomaj3 (3.1.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Lintian-override for debian-rules-sets-DEB_BUILD_OPTIONS since check
    should be really prevented
  * Lintian override for script-with-language-extension

 -- Andreas Tille <tille@debian.org>  Sun, 17 Jan 2021 09:48:10 +0100

biomaj3 (3.1.14-3) unstable; urgency=medium

  * Skip tests (need a database)

 -- Olivier Sallou <osallou@debian.org>  Sat, 08 Feb 2020 16:27:13 +0000

biomaj3 (3.1.14-2) unstable; urgency=medium

  * Remove influxdb stat feature due to python-influxdb bug #950063.
    Create new bug #950914 to track this issue. Once fixed, can revert back
    related patch.
  * Enable unit tests, disabling network related tests

 -- Olivier Sallou <osallou@debian.org>  Sat, 08 Feb 2020 09:47:29 +0000

biomaj3 (3.1.14-1) unstable; urgency=medium

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Tue, 12 Nov 2019 10:29:09 +0000

biomaj3 (3.1.8-1) unstable; urgency=medium

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Sat, 09 Mar 2019 10:18:52 +0000

biomaj3 (3.1.6-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Olivier Sallou ]
  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Thu, 25 Oct 2018 09:18:06 +0000

biomaj3 (3.1.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * Testsuite: autopkgtest-pkg-python
  * Fix bash path in example script

 -- Andreas Tille <tille@debian.org>  Fri, 08 Jun 2018 12:48:14 +0200

biomaj3 (3.1.3-1) unstable; urgency=low

  * First packaging of biomaj (Closes: #872451).

 -- Olivier Sallou <osallou@debian.org>  Thu, 17 Aug 2017 09:59:28 +0000
